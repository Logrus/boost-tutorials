#include <iostream> 
#include <boost\thread\thread.hpp>
int sum = 0; 

boost::mutex mutex;

void calculate() 
{ 
  for (int i = 0; i < 1000; ++i) 
    sum += i; 
} 

void print() 
{ 
	boost::lock_guard<boost::mutex> lock(mutex);
  std::cout << sum << std::endl; 
} 

void thread() 
{ 
  calculate(); 
  print();
  print(); 
} 

int main() 
{ 
  boost::thread t(thread);
  t.join();
} 
