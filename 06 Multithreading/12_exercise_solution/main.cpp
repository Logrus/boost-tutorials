#include <boost/date_time/posix_time/posix_time.hpp> 
#include <boost/cstdint.hpp>
#include <boost/thread/thread.hpp>
#include <iostream> 

// When exploiting thread func, boost allows to take only one param to func
// but I need 2
struct start_end
{
	int start, end;
};

// These should be global, as they are used among threads
boost::mutex mutex;
boost::uint64_t sum;

// This is function which sums ranges of numbers between
// param.start
// and
// param.end
void sum_range(start_end params)
{
	
	uint64_t count = 0;
	for (int i = params.start; i < params.end; ++i) 
		count+=i;

	boost::lock_guard<boost::mutex> lock(mutex); // We must not violate gobal variable
	sum += count;
}

int main() 
{ 
	// The task itself
	boost::posix_time::ptime start = boost::posix_time::microsec_clock::local_time(); 

	sum = 0; 
	for (int i = 0; i < 1000000000; ++i) 
		sum += i; 

	boost::posix_time::ptime end = boost::posix_time::microsec_clock::local_time(); 
	std::cout << end - start << std::endl; 

	std::cout << sum << std::endl; 


	// Solution
	std::cout << "Compared to (2 threads): \n";
	
	start = boost::posix_time::microsec_clock::local_time(); 

	sum = 0; start_end s;

	s.start = 0; 
	s.end   = 1000000000/2;
	boost::thread t1( sum_range, s );

	s.start = 1000000000/2;
	s.end   = 1000000000;
	boost::thread t2( sum_range, s );
	
	t1.join();
	t2.join();


	end = boost::posix_time::microsec_clock::local_time(); 
	std::cout << end - start << std::endl; 

	std::cout << sum << std::endl; 
	
	// Second solution
	int cores = boost::thread::hardware_concurrency();
	std::cout << "And compared to ("<<cores<<" threads): \n";
	boost::thread * threads = new boost::thread [cores];

	int one_thread_range = 1000000000/cores;

	start = boost::posix_time::microsec_clock::local_time(); 

	sum = 0;
	for (int i = 0; i < cores; ++i) 
	{
		s.start = i*one_thread_range; s.end = (i+1)*one_thread_range;
		threads[i] = boost::thread(sum_range, s);  
	}

	for (int i = 0; i < cores; ++i) 
		threads[i].join(); 

	end = boost::posix_time::microsec_clock::local_time(); 
	std::cout << end - start << std::endl; 

	std::cout << sum << std::endl; 
} 