cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(boost_threads)

find_package(PCL 1.6 REQUIRED)

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_executable (01_thread_management 01_thread_management/main.cpp)
target_link_libraries (01_thread_management ${PCL_LIBRARIES})

add_executable (02_interrupt 02_interrupt/main.cpp)
target_link_libraries (02_interrupt ${PCL_LIBRARIES})

add_executable (03_thread_info 03_thread_info/main.cpp)
target_link_libraries (03_thread_info ${PCL_LIBRARIES})

add_executable (04_synchronization 04_synchronization/main.cpp)
target_link_libraries (04_synchronization ${PCL_LIBRARIES})

add_executable (05_lock_guard 05_lock_guard/main.cpp)
target_link_libraries (05_lock_guard ${PCL_LIBRARIES})

add_executable (06_unique_lock 06_unique_lock/main.cpp)
target_link_libraries (06_unique_lock ${PCL_LIBRARIES})

add_executable (07_shared_mutex 07_shared_mutex/main.cpp)
target_link_libraries (07_shared_mutex ${PCL_LIBRARIES})

add_executable (08_condition_variable 08_condition_variable/main.cpp)
target_link_libraries (08_condition_variable ${PCL_LIBRARIES})

add_executable (09_thread_local_storage 09_thread_local_storage/main.cpp)
target_link_libraries (09_thread_local_storage ${PCL_LIBRARIES})

add_executable (10_tls_defect_fix 10_tls_defect_fix/main.cpp)
target_link_libraries (10_tls_defect_fix ${PCL_LIBRARIES})

add_executable (11_exercise_task 11_exercise_task/main.cpp)
target_link_libraries (11_exercise_task ${PCL_LIBRARIES})

add_executable (12_exercise_solution 12_exercise_solution/main.cpp)
target_link_libraries (12_exercise_solution ${PCL_LIBRARIES})

add_executable (13_exercise2_task 13_exercise2_task/main.cpp)
target_link_libraries (13_exercise2_task ${PCL_LIBRARIES})

add_executable (14_exercise2_solution 14_exercise2_solution/main.cpp)
target_link_libraries (14_exercise2_solution ${PCL_LIBRARIES})